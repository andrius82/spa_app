import React from 'react';
import servimg from '../images/Desert.jpg';


const Services = () => (
   <>
      <div className="section" >
        <h1>Services</h1>  
        <img src={servimg} className="img" alt="Home" />
      </div>
   </>
);


export default Services;
