import React from 'react';
import logo from '../logo.svg';
import '../scss/App.scss';
import Nav from './Nav';
import Home from './Home';
import Services from './Services';
import Contacts from './Contacts';
import PageNotFound from './PageNotFound';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="mainlogo" alt="logo" />
        <Router>
          <Nav/>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/services" component={Services} />
            <Route path="/contacts" component={Contacts} />
            <Route component={PageNotFound} />
          </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App;
