import React from 'react';


const PageNotFound = () => (
   <>
      <div className="section" >
        <h1>Page Not Found - 404 error</h1>  
      </div>
   </>
);


export default PageNotFound;
