import React from 'react';
import {
   Link,
 } from "react-router-dom";

const Nav = () => (
   <>
      <ul  className="mainnav" >
         <li className="mainnav__item"  >
            <Link to="/">Home</Link>
         </li>
         <li className="mainnav__item"  >
            <Link to="/services">Services</Link>
         </li>
         <li className="mainnav__item"  >
            <Link to="/contacts">Contacts</Link>
         </li>
      </ul>
   </>
);


export default Nav;
