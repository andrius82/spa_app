import React from 'react';
import homeimg from '../images/Lighthouse.jpg';


const Home = () => (
   <>
      <div className="section" >
        <h1>Home</h1>  
        <img src={homeimg} className="img" alt="Home" />
      </div>
   </>
);


export default Home;
